<?php

namespace txd\widgets\datetimepicker;

use Yii;
use yii\web\AssetBundle;

class MomentTimeZoneAsset extends AssetBundle
{
	/**
	 * @inheritdoc
	 */
	public $sourcePath = '@npm/moment-timezone';

	/**
	 * @inheritdoc
	 */
	public $js = [
		'builds/moment-timezone-with-data.min.js',
	];

	/**
	 * @inheritdoc
	 */
	public $depends = [
		'txd\widgets\datetimepicker\MomentAsset',
	];
}
