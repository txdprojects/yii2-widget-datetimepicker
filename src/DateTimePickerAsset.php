<?php

namespace txd\widgets\datetimepicker;

use yii\web\AssetBundle;

class DateTimePickerAsset extends AssetBundle
{
	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = '@npm/tempusdominus-bootstrap-4/build';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'css/tempusdominus-bootstrap-4.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/tempusdominus-bootstrap-4.min.js',
	];

	/**
	 * {@inheritdoc}
	 */
	public $depends = [
		'yii\web\JqueryAsset',
		'txd\widgets\datetimepicker\MomentAsset',
		'txd\widgets\datetimepicker\MomentTimeZoneAsset',
	];
}
