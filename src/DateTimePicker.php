<?php
namespace txd\widgets\datetimepicker;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\InputWidget;

/**
 * Class DateTimePicker
 *
 * @link https://tempusdominus.github.io/bootstrap-4/Usage/
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class DateTimePicker extends InputWidget
{
	const INPUT_ADDON_PREPEND = 'prepend';
	const INPUT_ADDON_APPEND = 'append';

	/**
	 * @var bool|string The input addon
	 */
	public $inputAddon = self::INPUT_ADDON_PREPEND;

	/**
	 * @var string The input addon content
	 */
	public $inputAddonContent = '<span class="fal fa-calendar"></span>';

	/**
	 * @var string The linked DateTimePicker widget selector
	 */
	public $linkedTo;

	/**
	 * @var array The container options
	 */
	public $containerOptions = [];

	/**
	 * @var array The client (JS) options
	 */
	public $clientOptions = [];

	/**
	 * @var array The client (JS) events
	 */
	public $clientEvents = [];

	/**
	 * @var string The client (JS) selector
	 */
	private $_clientSelector;

	/**
	 * @var string The global widget JS hash variable
	 */
	private $_hashVar;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->setupProperties();
		$this->registerAssets();
	}

	/**
	 * {@inheritdoc}
	 */
	public function run()
	{
		$content = [];
		$content[] = Html::beginTag('div', $this->containerOptions);
		if (!$this->clientOptions['inline']) {
			$content[] = $this->renderInputHtml('text');
		}
		$content[] = Html::endTag('div');

		if (!$this->clientOptions['inline'] && $this->inputAddon) {
			if ($this->inputAddon === self::INPUT_ADDON_PREPEND) {
				array_splice($content, 1, 0, $this->renderInputAddon(self::INPUT_ADDON_PREPEND));
			} else {
				array_splice($content, 2, 0, $this->renderInputAddon(self::INPUT_ADDON_APPEND));
			}
		}

		return implode("\n", $content);
	}

	/**
	 * Gets the client selector.
	 *
	 * @return string
	 */
	public function getClientSelector()
	{
		if (!$this->_clientSelector) {
			$this->_clientSelector = '#' . $this->getId();
		}
		return $this->_clientSelector;
	}

	/**
	 * Gets the hash variable.
	 *
	 * @return string
	 */
	public function getHashVar()
	{
		if (!$this->_hashVar) {
			$this->_hashVar = 'datetimepicker_' . hash('crc32', $this->buildClientOptions());
		}
		return $this->_hashVar;
	}

	/**
	 * Sets the widget properties.
	 */
	protected function setupProperties()
	{
		if ($this->hasModel()) {
			$this->setId(Html::getInputId($this->model, $this->attribute) . '-datetimepicker');
		} else {
			// Ensure that input does not have an id if no model is attached
			$this->options['id'] = null;
		}

		// Use the container ID for widget if is set
		if (isset($this->containerOptions['id'])) {
			$this->setId($this->containerOptions['id']);
		} else {
			$this->containerOptions['id'] = $this->getId();
		}

		$this->options = ArrayHelper::merge([
			'class' => 'form-control',
			'autocomplete' => 'off',
			'data' => [
				'datetimepicker-options' => $this->getHashVar(),
			],
		], $this->options);

		$this->containerOptions['data-target-input'] = 'nearest';
		Html::addCssClass($this->containerOptions, 'datetimepicker');
		if ($this->inputAddon) {
			Html::addCssClass($this->containerOptions, 'input-group');
		}

		Html::addCssClass($this->options, 'datetimepicker-input');
		$this->options['data-target'] = "#{$this->getId()}";
	}

	/**
	 * Builds the client options.
	 *
	 * @return string
	 */
	protected function buildClientOptions()
	{
		$defaultClientOptions = [
			'format' => 'YYYY-MM-DD HH:mm:ss',
			'locale' => substr(Yii::$app->language, 0, 2),
		];

		$clientOptions = ArrayHelper::merge($defaultClientOptions, $this->clientOptions);

		// Convert date formats
		if (strncmp($clientOptions['format'], 'icu:', 4) === 0) {
			$clientOptions['format'] = MomentFormat::convertDateIcuToMoment(substr($clientOptions['format'], 4));
		} elseif (strncmp($clientOptions['format'], 'php:', 4) === 0) {
			$clientOptions['format'] = MomentFormat::convertDatePhpToMoment(substr($clientOptions['format'], 4));
		}

		$this->clientOptions = $clientOptions;

		return Json::encode($clientOptions);
	}

	/**
	 * Registers the widget assets.
	 */
	protected function registerAssets()
	{
		$view = $this->getView();

		// Register assets
		DateTimePickerAsset::register($view);

		// Register widget hash JavaScript variable
		$view->registerJs("var {$this->getHashVar()} = {$this->buildClientOptions()};", View::POS_HEAD);

		// Build client script
		$js = "";
		if (isset($this->clientOptions['firstDayOfWeek'])) {
			$js .= "moment.updateLocale('{$this->clientOptions['locale']}', {week: {dow: {$this->clientOptions['firstDayOfWeek']}}});";
		}
		$js .= "jQuery('{$this->getClientSelector()}').datetimepicker({$this->getHashVar()})";

		// Build client events
		if (!empty($this->clientEvents)) {
			foreach ($this->clientEvents as $clientEvent => $eventHandler) {
				if (!($eventHandler instanceof JsExpression)) {
					$eventHandler = new JsExpression($eventHandler);
				}
				$js .= ".on('{$clientEvent}', {$eventHandler})";
			}
		}

		// Register widget JavaScript
		$view->registerJs("{$js};");

		// Check if this widget is linked to another DateTimePicker
		if (!empty($this->linkedTo)) {
			// Register custom JS event to set this widget minDate client option
			$view->registerJs('
				var $currentDtp = jQuery("' . $this->getClientSelector() . '");
				jQuery("' . $this->linkedTo .'").on("change.datetimepicker", function (e) {
					$currentDtp.datetimepicker("minDate", e.date);
					if ($currentDtp.datetimepicker("date") && e.date && e.date.isAfter($currentDtp.datetimepicker("date"))) {
						$currentDtp.datetimepicker("date", e.date);
					}
				});
			');
		}
	}

	/**
	 * Renders a Bootstrap input group addon.
	 *
	 * @return string
	 */
	protected function renderInputAddon($position = self::INPUT_ADDON_PREPEND)
	{
		return Html::tag('div', Html::tag('div', $this->inputAddonContent, ['class' => 'input-group-text']), [
			'class' => "input-group-{$position}",
			'data' => [
				'toggle' => 'datetimepicker',
				'target' => "#{$this->getId()}",
			],
		]);
	}
}
